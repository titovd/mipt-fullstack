from rest_framework import viewsets, mixins
from .models import Post
from .serializers import PostSerializer


class PostViewSet(viewsets.GenericViewSet,
                    mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.CreateModelMixin):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


    def perform_create(self, serializer):
        serializer.save(author=self.request.user)



class PostSelfViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
  

    def get_queryset(self):
        queryset = super(PostSelfViewSet, self).get_queryset()
        return queryset.filter(author=self.request.user)