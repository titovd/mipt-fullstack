from rest_framework import serializers
from .models import Post
from core.serializers import UserSerializer
from comment.serializers import CommentSerializer

class PostSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    comment_set = CommentSerializer(many=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'description', 'text', 'author', 'comment_set')
        read_only_fields = ['author']

