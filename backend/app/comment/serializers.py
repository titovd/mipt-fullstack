from rest_framework import serializers
from core.serializers import UserSerializer
from .models import Comment

class CommentSerializer(serializers.ModelSerializer):
	author = UserSerializer(read_only=True)

	class Meta:
		model = Comment
		fields = ('id', 'text', 'post', 'author')