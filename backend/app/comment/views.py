from rest_framework import viewsets
from .models import Comment
from .serializers import CommentSerializer


class СommentSelfViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
  

    def get_queryset(self):
        queryset = super(СommentSelfViewSet, self).get_queryset()
        return queryset.filter(author=self.request.user)