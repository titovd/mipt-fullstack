import { FETCH_POSTS_SUCCESS } from '../actions/posts'
import { FETCH_POST_SUCCESS } from '../actions/posts'
import { FETCH_POSTS } from '../actions/posts'

const initialState: IPostState = {
    postList: [],
    posts: {},
    isLoading: false
}

interface IPostAction {
    type: string
    payload: {
        result: number[] | number,
        entities: { post: { [key: number]: IPost } },
    }
}

export default function (state = initialState, action: IPostAction) {
    switch (action.type) {
        case FETCH_POSTS:
            return {
                ...state,
                isLoading: true
            }

        case FETCH_POSTS_SUCCESS:
            return {
                ...state,
                posts: action.payload.entities.post,
                postList: action.payload.result as number[],
                isLoading: false
            }
        case FETCH_POST_SUCCESS:
            return {
                ...state,
                posts: {
                    ...state.posts,
                    ...action.payload.entities.post,
                },
                postList: [action.payload.result as number],
                isLoading: false
            }
    }
    return state
}