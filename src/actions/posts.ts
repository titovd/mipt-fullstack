import { postsNormalize, postNormalize } from '../shemas/posts'
import { Dispatch } from 'redux'
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS'
export const FETCH_POSTS = 'FETCH_POSTS'
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS'

export function fetchPosts() {
    return async (dispatch: Dispatch) => {
        dispatch({
            type: FETCH_POSTS
        })
        const response = await fetch('http://localhost:3000/posts')
        const data = await response.json()

        dispatch({
            type: FETCH_POSTS_SUCCESS,
            payload: postsNormalize(data)
        })
    }
}

export function fetchPost(postId: string) {
    return async (dispatch: Dispatch) => {
        dispatch({
            type: FETCH_POSTS
        })
        const response = await fetch(`http://localhost:3000/posts/${postId}`)
        const data = await response.json()
        dispatch({
            type: FETCH_POST_SUCCESS,
            payload: postNormalize(data)
        })
    }
}