import React from 'react'
import spinningPic from '../../assets/images/spinning-pic.jpg'
import spinningPic1 from '../../assets/images/spinning-pic1.jpg'
import spinningPic2 from '../../assets/images/spinning-pic2.jpg'
import Loader from '../Loader'

export interface IPostStateProps {
    isLoading: boolean
    post: IPost
}

export interface IPostDispatchProps {
    fetchPost: (postId: string) => void
}

export interface IPostOwnProps {
    match: {
        params: {
            postId: string
        }
    }
}

interface IPostProps
    extends IPostStateProps,
    IPostDispatchProps,
    IPostOwnProps { }

const Post: React.FC<IPostProps> = (props) => {
    const { fetchPost, match, isLoading, post } = props

    React.useEffect(() => {
        fetchPost(match.params.postId)
    }, [])

    if (isLoading || !post) {
        return <Loader />
    }

    const { title, description, text } = post
    return (
        <section className="news-board">
            <div className="news-board-item">
                <div className="news-board-item-title">
                    {title}
                </div>
                <img className="news-board-item-pic" src={spinningPic} alt="Спиннинг" />
                <div className="news-board-item-content">
                    {description}
                    <img className="news-board-item-pic" src={spinningPic1} alt="Спиннинг" />
                    {text}
                    <img className="news-board-item-pic" src={spinningPic2} alt="Спиннинг" />
                </div>
            </div>
        </section>
    )
}

export default Post;