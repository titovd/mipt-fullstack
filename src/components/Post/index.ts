import { connect } from 'react-redux'
import { fetchPost } from '../../actions/posts'
import Post, {
    IPostStateProps,
    IPostDispatchProps,
    IPostOwnProps
} from './Post'


function mapStateToProps(
    state: IState,
    props: IPostOwnProps
): IPostStateProps {
    return {
        post: state.posts.posts[Number(props.match.params.postId)],
        isLoading: state.posts.isLoading
    }
}

const mapDispatchToProps: IPostDispatchProps = {
    fetchPost
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Post)