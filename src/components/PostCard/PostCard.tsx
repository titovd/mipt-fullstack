import React from 'react'
import { Link } from 'react-router-dom'
import spinningPic from '../../assets/images/spinning-pic.jpg'

export interface IPostStateCardProps {
    post: IPost
}

export interface IPostOwnCardProps {
    postId: number
}

interface IPostCardProps
    extends IPostStateCardProps,
    IPostOwnCardProps { }


const PostCard: React.FC<IPostCardProps> = (props) => {
    const { post } = props
    if (!post) {
        return null
    }
    const { title, description, id } = post
    return (
        <div className="news-board-item">
            <Link to={`/post/${id}`}>
                <div className="news-board-item-title">
                    {title}
                </div>
            </Link>
            <img className="news-board-item-pic" src={spinningPic} alt="Спиннинг" />
            <div className="news-board-item-content">
                {description}
            </div>
        </div>
    )
}

export default PostCard