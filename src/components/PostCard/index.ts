import { connect } from 'react-redux'
import PostCard, {
    IPostOwnCardProps,
    IPostStateCardProps
} from './PostCard'

function mapStateToProps(
    state: IState,
    props: IPostOwnCardProps
): IPostStateCardProps {
    const { postId } = props

    return {
        post: state.posts.posts[postId]
    }
}

export default connect(
    mapStateToProps
)(PostCard)