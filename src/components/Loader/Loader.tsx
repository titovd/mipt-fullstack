import React from 'react'

const Loader: React.FC = () => {
    return (
        <div className="loader">Пожалуйста, подождите...</div>
    )
}

export default Loader