import { connect } from 'react-redux'
import { fetchPosts } from '../../actions/posts'
import NewsBoard from './NewsBoard'

function mapStateToProps(state: IState) {
    return {
        postList: state.posts.postList,
        isLoading: state.posts.isLoading
    }
}

const mapDispatchToProps = {
    fetchPosts
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewsBoard)