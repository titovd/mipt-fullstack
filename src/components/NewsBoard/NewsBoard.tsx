import React from 'react'
import PostCard from '../PostCard'
import Loader from '../Loader'

interface INewsBoardProps {
	fetchPosts: () => void
	postList: number[]
	isLoading: boolean
}

const NewsBoard: React.FC<INewsBoardProps> = (props) => {
	const { fetchPosts, postList, isLoading } = props

	React.useEffect(() => {
		fetchPosts()
	}, [])

	if (isLoading || !postList) {
		return <Loader />
	}

	return (
		<section className="news-board">
			{postList.map((postId, index) => (
				<PostCard key={index} postId={postId} />
			))}
		</section>
	);
}

export default NewsBoard