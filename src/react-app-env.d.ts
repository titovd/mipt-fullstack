/// <reference types="react-scripts" />

interface IPost {
    title: string
    description: string
    id: string
    text: string
}

interface IPostState {
    postList: number[]
    posts: { [key: number]: IPost },
    isLoading: boolean
}

interface IState {
    posts: IPostState
}